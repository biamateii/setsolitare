#pragma once
#include <fstream>
#include "Player.h"
#include "Dealer.h"

class SetGame
{
public:
	SetGame();
	SetGame(const Dealer &dealer, 
		const Deck &deck, 
		const BoardGame &boardGame);
	~SetGame() = default;

	void play();

private:
	bool isSet(const Card &firstCard,
		const Card &secondCard,
		const Card &thirdCard) const;
	std::vector<size_t> findSet() const;
	bool isGameOver() const;
	void printIsSet(std::ostream &os, const Player *player) const;
	void printNotSet(std::ostream &os, const Player *player) const;


	std::vector<Player*> m_arrayOfPlayers;
	Dealer m_dealer;
	BoardGame m_boardGame;
	Deck m_deck;
};

