#include "Player.h"

const size_t maxNumberOfCardsInTheSet = 3;

Player::Player(const std::string &name) :
	m_playerPoints(0),
	m_name(name)
{
}

void Player::setPlayerPoints(const int & points)
{
	int currentScore = m_playerPoints + points;
	if (currentScore >= 0)
		m_playerPoints = currentScore;
}
size_t Player::getPlayerPoints() const
{
	return m_playerPoints;
}
