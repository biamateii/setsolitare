#include "Deck.h"
#include <random>

template< typename T >
typename size_t begin()
{
	return static_cast<size_t>(T::First);
}

template< typename T >
typename size_t end()
{
	return static_cast<size_t>(T::Last);
}

Deck::Deck(const std::string &name): 
	m_indexCard(0), 
	m_name(name)
{
	for (size_t indexNumber = begin<Card::Number>();
				indexNumber <= end<Card::Number>();
				indexNumber++)
	{
		for (size_t indexSymbol = begin<Card::Symbol>();
					indexSymbol <= end<Card::Symbol>();
					indexSymbol++)
		{
			for (size_t indexShading = begin<Card::Symbol>();
						indexShading <= end<Card::Symbol>();
						indexShading++)
			{
				for (size_t indexColor = begin<Card::Symbol>();
							indexColor <= end<Card::Symbol>();
							indexColor++)
				{
					Card newCard(static_cast<Card::Number>(indexNumber),
								static_cast<Card::Symbol>(indexSymbol),
								static_cast<Card::Shading>(indexShading),
								static_cast<Card::Color>(indexColor));
					m_deck.push_back(newCard);
				}
			}
		}
	}
}
Deck::Deck(const Deck &otherDeck) :
	m_indexCard(0)
{
	m_deck = otherDeck.m_deck;
}

void Deck::shuffleDeck()
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(0, maxSizeOfDeck-1);

	for (auto&& elem : m_deck)
	{
		unsigned int randomCardIndex = dis(gen);
		std::swap(elem, m_deck.at(randomCardIndex));
	}
}

void Deck::printDeck() const
{
	for (const auto& elem : m_deck)
	{
		std::cout << elem << std::endl;
	}
}

Card& Deck::dealCard()
{
	if (m_indexCard < maxSizeOfDeck)
	{
		return m_deck.at(m_indexCard++);
	}
	throw std::exception("No more cards in the deck");
}

size_t Deck::getCurrentNumbetOfCards() const
{
	return m_indexCard;
}
