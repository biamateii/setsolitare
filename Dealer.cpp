#include "Dealer.h"

const size_t maxSizeOfBoardGame = 12;

Dealer::Dealer():Dealer("Default name")
{
}

Dealer::Dealer(const std::string &name) :
	m_name(name)
{
}

void Dealer::fillTheBoardWithCards(
	BoardGame &boardGame,
	Deck &deck)
{
	boardGame.fillWithCards(deck);
}

void Dealer::removeCards(
	BoardGame &boardGame,
	const std::vector<size_t> &index)
{
	boardGame.setBoardGame(index);
}

void Dealer::addCards(
	BoardGame &boardGame, 
	Deck &deck)
{

	boardGame.addNewCards(deck);

}