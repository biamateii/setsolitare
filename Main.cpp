#pragma once
#include <random>
#include "SetGame.h"

std::string getColor()
{
	char hex[] = { 'A', 'B', 'C', 'D', 'E', 'F', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

	std::string color;
	color = color+ "#";
	for (auto index = 0; index < 6; index++)
	{
		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_int_distribution<> dis(0,15);

		color.push_back(hex[dis(gen)]);
	}

	return color;
}

int main()
{
	// random generation of a color in hexadecimal
	//std::cout << getColor();

	SetGame newGame;
	newGame.play();

	return 0;
}