#pragma once
#include <iostream>
#include "CardFeatures.h"
#include <string>

class Card
{
public:

	enum class Number { DEFAULT, ONE, TWO, THREE, First = ONE, Last = THREE };
	enum class Symbol { DEFAULT, DIAMOND, SQUIGGLE, OVAL, First = DIAMOND, Last = OVAL};
	enum class Shading { DEFAULT, SOLID, STRIPED, OPEN, First = SOLID, Last = OPEN};
	enum class Color { DEFAULT, RED, GREEN, BLUE, First = RED, Last = BLUE};

	Card();
	Card(const Number &number,
		const Symbol &symbol,
		const Shading &shading,
		const Color &color);
	Card(const Card &otherCard);

	~Card() = default;

	friend std::ostream& operator<<(std::ostream &os, const Card &card);
	Card& operator=(const Card &otherCard);
	bool operator==(const Card &otherCard) const;
	bool operator!=(const Card &otherCard) const;

	Number getNumber() const { return m_number; }
	Shading getShading() const { return m_shading; }
	Symbol getSymbol() const { return m_symbol; }
	Color getColor() const { return m_color; }


private:
	Color m_color;
	Number m_number;
	Shading m_shading;
	Symbol m_symbol;

	std::string printCardNumber() const;
	std::string printCardShading() const;
	std::string printCardSymbol() const;
	std::string printCardColor() const;
};