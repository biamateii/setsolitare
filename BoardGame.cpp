#include "BoardGame.h"

size_t BoardGame::numberOfCards = 12;

BoardGame::BoardGame(const std::string& name)
	:m_name(name)
{
	for (auto index = 0; index < numberOfCards; index++)
	{
		m_boardGame.push_back(Card());
	}
}
BoardGame::BoardGame(const BoardGame &otherBoardGame)
{
	m_boardGame = otherBoardGame.m_boardGame;
}

void BoardGame::fillWithCards(Deck &deck)
{
	for (auto &&elem : m_boardGame)
	{
		if (elem == Card())
		{
			elem = deck.dealCard();
		}
	}
}

void BoardGame::setBoardGame(const std::vector<size_t> &index)
{
	m_boardGame[index[0]] = Card();
	m_boardGame[index[1]] = Card();
	m_boardGame[index[2]] = Card();
}

Card BoardGame::getCard(const size_t &index) const
{
	return m_boardGame[index];
}

void BoardGame::addNewCards(Deck &deck)
{
	try
	{
		Card newCard = deck.dealCard();
		m_boardGame.push_back(newCard);
		numberOfCards++;

		newCard = deck.dealCard();
		m_boardGame.push_back(newCard);
		numberOfCards++;

		newCard = deck.dealCard();
		m_boardGame.push_back(newCard);
		numberOfCards++;
	}
	catch (const std::exception&)
	{
		std::cerr << std::exception().what();

	}
}

void BoardGame::print() const
{
	const size_t maxNumberOfColumns = 3;
	for (auto index = 0; index < numberOfCards; index++)
	{
		std::cout << m_boardGame[index] << " ";
		if ((index + 1) % maxNumberOfColumns == 0)
			std::cout << std::endl;
	}
}
