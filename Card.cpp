#include "Card.h"



Card::Card() :
	m_number(Number::DEFAULT),
	m_symbol(Symbol::DEFAULT),
	m_shading(Shading::DEFAULT),
	m_color(Color::DEFAULT)
{
}
Card::Card(const Number &number, const Symbol &symbol, const Shading &shading, const Color &color) :
	m_number(number),
	m_symbol(symbol),
	m_shading(shading),
	m_color(color)
{
}
Card::Card(const Card &other)
{
	*this = other;
}

std::string Card::printCardNumber() const
{
	switch (m_number)
	{
	case Card::Number::ONE:
		return "1";
	case Card::Number::TWO:
		return "2";
	case Card::Number::THREE:
		return "3";
	default:
		return " ";
	}
}
std::string Card::printCardShading() const
{
	switch (m_shading)
	{
	case Card::Shading::SOLID:
		return "Solid";
	case Card::Shading::STRIPED:
		return "Striped";
	case Card::Shading::OPEN:
		return "Open";
	default:
		return "";
	}
}
std::string Card::printCardSymbol() const
{
	switch (m_symbol)
	{
	case Card::Symbol::DIAMOND:
		return "Diamond";
	case Card::Symbol::SQUIGGLE:
		return "Squiggle";
	case Card::Symbol::OVAL:
		return "Oval";
	default:
		return "";
	}
}
std::string Card::printCardColor() const
{
	switch (m_color)
	{
	case Card::Color::RED:
		return "Red";
	case Card::Color::GREEN:
		return "Green";
	case Card::Color::BLUE:
		return "Blue";
	default:
		return "";
	}
}

std::ostream& operator<<(std::ostream &os, const Card &card)
{
	os << card.printCardNumber() << "_"
		<< card.printCardSymbol() << "_"
		<< card.printCardShading() << "_"
		<< card.printCardColor() << " ";

	return os;
}
Card& Card::operator=(const Card &otherCard)
{
	m_number = otherCard.m_number;
	m_symbol = otherCard.m_symbol;
	m_shading = otherCard.m_shading;
	m_color = otherCard.m_color;

	return *this;
}
bool Card::operator==(const Card &otherCard) const
{
	return m_number == otherCard.m_number && 
		   m_shading == otherCard.m_shading && 
		   m_symbol == otherCard.m_symbol && 
		   m_color == otherCard.m_color;
}
bool Card::operator!=(const Card &otherCard) const
{
	return m_number != otherCard.m_number ||
		m_shading != otherCard.m_shading ||
		m_symbol != otherCard.m_symbol ||
		m_color != otherCard.m_color;
}