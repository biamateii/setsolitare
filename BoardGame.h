#pragma once
#include "Deck.h"

class BoardGame
{
public:
	BoardGame(const std::string& name = "Default name");
	BoardGame(const BoardGame &otherBoardGame);
	~BoardGame() = default;

	void fillWithCards(Deck &deck);
	void setBoardGame(const std::vector<size_t> &index);
	Card getCard(const size_t &index1) const;
	void addNewCards(Deck &deck);
	void print() const;

	static size_t numberOfCards;
private:
	std::vector<Card> m_boardGame;
	std::string m_name;
};

