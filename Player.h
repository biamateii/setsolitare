#pragma once
#include "Card.h"
#include "BoardGame.h"
#include <vector>
#include <string>

class Player
{
public:
	Player(const std::string &name="Default name");
	~Player() = default;

	std::string getName() const { return m_name; }
	void setPlayerPoints(const int &points);
	size_t getPlayerPoints() const;

private:
	std::string m_name;
	size_t m_playerPoints;
};

