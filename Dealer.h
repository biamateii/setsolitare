#pragma once
#include "Deck.h"
#include "BoardGame.h"
#include <string>

class Dealer
{
public:
	Dealer();
	Dealer(const std::string &name);
	~Dealer() = default;

	void fillTheBoardWithCards(
		BoardGame &boardGame, 
		Deck &deck);
	void removeCards(
		BoardGame &boardGame, 
		const std::vector<size_t> &index);
	void addCards(
		BoardGame &boardGame, 
		Deck &deck);

private:
	std::string m_name;
};

