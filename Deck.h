#pragma once
#include "Card.h"
#include <vector>
#include <string>

class Deck
{
public:
	Deck(const std::string &name = "Default name");
	Deck(const Deck &otherDeck);
	~Deck() = default;

	void shuffleDeck();
	void printDeck() const;
	Card& dealCard();

	size_t getCurrentNumbetOfCards() const;

	const size_t maxSizeOfDeck = 81;
private:
	std::vector<Card> m_deck;
	size_t m_indexCard;
	std::string m_name;
};

