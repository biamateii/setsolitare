#include "SetGame.h"

SetGame::SetGame() :
	SetGame(Dealer(), Deck(), BoardGame())
{
}

SetGame::SetGame(const Dealer &dealer,
	const Deck &deck,
	const BoardGame &boardGame) :
	m_dealer(dealer),
	m_deck(deck),
	m_boardGame(boardGame)
{
}

template<typename T>
bool isEqual(const T &firstCard, const T &secondCard, const T &thirdCard)
{
	return (static_cast<int>(firstCard) !=
		static_cast<int>(secondCard) &&
		static_cast<int>(firstCard) !=
		static_cast<int>(thirdCard) &&
		static_cast<int>(secondCard) !=
		static_cast<int>(thirdCard)) ||
		(static_cast<int>(thirdCard) ==
			static_cast<int>(firstCard) &&
			static_cast<int>(secondCard) ==
			static_cast<int>(thirdCard));
}

bool isValid(const Card &firstCard, const Card &secondCard, const Card &thirdCard)
{
	return firstCard != Card() &&
		secondCard != Card() &&
		thirdCard != Card();
}

bool SetGame::isSet(const Card &firstCard, const Card &secondCard, const Card &thirdCard) const
{
	return isEqual<Card::Number>(firstCard.getNumber(),
		secondCard.getNumber(),
		thirdCard.getNumber()) &&
		isEqual<Card::Shading>(firstCard.getShading(),
			secondCard.getShading(),
			thirdCard.getShading()) &&
		isEqual<Card::Symbol>(firstCard.getSymbol(),
			secondCard.getSymbol(),
			thirdCard.getSymbol()) &&
		isEqual<Card::Color>(firstCard.getColor(),
			secondCard.getColor(),
			thirdCard.getColor()) &&
		isValid(firstCard, secondCard, thirdCard);

}

std::vector<size_t> SetGame::findSet() const
{
	for (size_t firstIndex = 0;
		firstIndex < m_boardGame.numberOfCards - 2;
		firstIndex++)
	{
		for (size_t secondIndex = firstIndex + 1;
			secondIndex < m_boardGame.numberOfCards - 1;
			secondIndex++)
		{
			for (size_t thirdIndex = secondIndex + 1;
				thirdIndex < m_boardGame.numberOfCards;
				thirdIndex++)
			{
				if (isSet(m_boardGame.getCard(firstIndex),
					m_boardGame.getCard(secondIndex),
					m_boardGame.getCard(thirdIndex)))
				{

					return std::vector<size_t>{firstIndex, secondIndex, thirdIndex};
				}
			}
		}

	}
	throw std::exception("no set");
}

void menu(const std::vector<Player*> &arrayOfPlayers)
{
	std::cout << "Enter your number and the word 'set' to pick a set or 'no' if you don't want to pick a set\n";
	size_t index = 1;
	for (auto &&elem : arrayOfPlayers)
		std::cout << index++ << " -> " << elem->getName() << std::endl;
}

bool SetGame::isGameOver() const
{
	if (m_deck.getCurrentNumbetOfCards() == m_deck.maxSizeOfDeck)
		return true;

	return false;
}

void SetGame::play()
{
	size_t noPlayers;
	std::cout << "How many players? :";
	std::cin >> noPlayers;

	for (auto index = 0; index < noPlayers; index++)
	{
		std::string name;
		std::cout << "Name player" << index + 1 << ": ";
		std::cin >> name;

		m_arrayOfPlayers.push_back(new Player(name));
	}

	m_deck.shuffleDeck();
	m_dealer.fillTheBoardWithCards(m_boardGame, m_deck);

	std::ofstream fout;
	fout.open("FileHistoryLog.txt");

	while (isGameOver() == false)
	{
		system("cls");
		menu(m_arrayOfPlayers);
		m_boardGame.print();
		size_t playerIndex;
		std::string playerChoice;
		std::cout << "-> ";
		std::cin >> playerIndex;
		std::cin >> playerChoice;

		playerIndex--;
		if (playerChoice == "set")
		{
			try
			{
				std::vector<size_t> set = findSet();
				m_arrayOfPlayers[playerIndex]->setPlayerPoints(1);
				m_dealer.removeCards(m_boardGame, set);
				m_dealer.fillTheBoardWithCards(m_boardGame, m_deck);
				printIsSet(fout, m_arrayOfPlayers[playerIndex]);
			}
			catch (const std::exception&)
			{
				//std::cerr << std::exception().what();
				m_arrayOfPlayers[playerIndex]->setPlayerPoints(-2);
				m_dealer.addCards(m_boardGame, m_deck);
				printNotSet(fout, m_arrayOfPlayers[playerIndex]);
			}
		}
	}

	fout.close();

	for (auto &&player : m_arrayOfPlayers)
	{
		std::cout << "Player " << player->getName()
			<< " has " << player->getPlayerPoints()
			<< " points\n";
	}

	for (auto &&player : m_arrayOfPlayers)
		delete player;

}

void SetGame::printIsSet(std::ostream &os, const Player *player) const
{
	std::vector<size_t> set = findSet();
	os << "Player " << player->getName()
		<< " has " << player->getPlayerPoints()
		<< " points, he found the set: " << std::endl;
	os << m_boardGame.getCard(set[0]) << " "
		<< m_boardGame.getCard(set[1]) << " "
		<< m_boardGame.getCard(set[2]) << std::endl << std::endl;

}

void SetGame::printNotSet(std::ostream & os, const Player * player) const
{
	os << "Player " << player->getName()
		<< " didn't find any set, he has now " << player->getPlayerPoints()
		<< " points" << std::endl << std::endl;
}
